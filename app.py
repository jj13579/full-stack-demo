from flask import Flask, render_template, request
from wtforms import Form, TextField
import requests, json 
app = Flask(__name__)


class BasicForm(Form):
	city = TextField('City: ')
	checkin = TextField('Checkin: ')
	checkout = TextField('Checkout: ')


@app.route('/', methods=['GET', 'POST'])
def basicInfo():
  form = BasicForm(request.form)

  if request.method == 'POST':
    city = request.form['city']
    checkin = request.form['checkin']
    checkout = request.form['checkout']
    dataToDisplay = sendInfoAndGetResults(city, checkin, checkout)
    # not sure if this was a sufficient caching mechanism, or we were to store all requests as long as the server is alive (i.e. K/V based on input)
    return render_template('showData.html', dataToDisplay=dataToDisplay) 
  else:
  	# better method here would be to selectively show data after submitting on the main demo.html page, to avoid having 2 separate pages on the same link
    return render_template('demo.html', form=form)


def sendInfoAndGetResults(city, checkin, checkout):
  infoForSnapTravel = {
    'city': city,
    'checkin': checkin,
    'checkout': checkout,
    'provider': 'snaptravel'
  }

  infoForHotels = {
    'city': city,
    'checkin': checkin,
    'checkout': checkout,
    'provider': 'retail'
  }

  data1 = json.loads(requests.post("https://experimentation.snaptravel.com/interview/hotels", data = infoForSnapTravel).text)['hotels']
  data2 = json.loads(requests.post("https://experimentation.snaptravel.com/interview/hotels", data = infoForHotels).text)['hotels']

  sortedData1 = sorted(data1, key = lambda i: i['id']) 
  sortedData2 = sorted(data2, key = lambda i: i['id']) 
  dataToDisplay = []

  i, j = 0, 0
  while i < len(sortedData1) and j < len(sortedData2):
    if sortedData1[i]['id'] < sortedData2[j]['id']:
      i += 1
    elif sortedData1[i]['id'] == sortedData2[j]['id']:
      temp = sortedData1[i]
      temp['hotelsPrice'] = sortedData2[j]['price']	
      dataToDisplay.append(temp)
      i += 1
    else:
      j += 1

  return dataToDisplay
